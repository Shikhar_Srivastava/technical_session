# Best Practices in JS
## Best Practices: Indentation

- Make sure that your code indentation is neat and good. 
- Provide easy to read and clear understandable code.

#### Bad Indentation:
```
function number(){
let x = 2;
console.log(x);
} function sqr() {
console.log(x*x);
  }
}
}

```

#### Good Indentation:
```
function number() {
  let x = 2;
  console.log(x);
}

function square() {
    let squareOfX = x * x;
    console.log(squareOfX);
}
```

## Best Practices: Variable Naming

- Make sure variable names are descriptive and brief.

#### Bad Naming:
```
const a = [1, 2, 3, 4, 5];
const a1 = [1, 4, 9, 16, 25];
```

#### Good Naming:
```
const numbers = [1, 2, 3, 4, 5];
const squares = [1, 4, 9, 16, 25];
```

## Best Practices: Clean Code

- Use meaningful names.

#### Bad Naming:
```
const foo = "JDoe@example.com";
const bar = "John";
const age = 23;
const qux = true;
```

#### Good Naming:
```
const email = "John@example.com";
const firstName = "John";
const age = 23;
const isActive = true
```

- Use multiple parameters over single object parameter.

#### Bad Parameters:
```
function CustomerDetail (User){    
    console.log('This is ${User.CustomerName} of ${User.CustomerType} and need ${User.Order}');
```

#### Good Parameters:
```
function CustomerDetail (CustomerName, CustomerType, Order){
    console.log('This is ${CustomerName} of ${CustomerType} and need ${Order}');
```

- Use non-negative conditionals.

####  Bad Conditionals:
```
function isUserNotVerified(user) {
  
}

if (!isUserNotVerified(user)) {
  
}
```

#### Good Conditionals:
```
function isUserVerified(user) {
  
}

if (isUserVerified(user)) {
  
}
```

- Use comments to make your code more readable.
- Don't use excessive comments.

#### Bad Commenting:
```
function generateHash(str) {
  // Hash variable
  let hash = 0;

  // Get the length of the string
  let length = str.length;

  // If the string is empty return
  if (!length) {
    return hash;
  }

  // Loop through every character in the string
  for (let i = 0; i < length; i++) {
    // Get character code.
    const char = str.charCodeAt(i);

    // Make the hash
    hash = (hash << 5) - hash + char;

    // Convert to 32-bit integer
    hash &= hash;
  }
}
```

#### Good Commenting:
```
function generateHash(str) {
  let hash = 0;
  let length = str.length;
  if (!length) {
    return hash;
  }

  for (let i = 0; i < length; i++) {
    const char = str.charCodeAt(i);
    hash = (hash << 5) - hash + char;
    hash = hash & hash; // Convert to 32bit integer
  }
  return hash;
}
```

Make sure that you write easy to read and understandable code. 

It is very important to use proper measure while writing code and follow proper steps.

## References
- [https://courses.cs.washington.edu/courses/cse154/18au/resources/styleguide/js/spacing-indentation-js.html]

- [https://medium.com/geekculture/writing-clean-javascript-es6-edition-834e83abc746]

 
